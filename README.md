# colliaziun

A jQuery plugin for adding stylable connecting lines between block elements.

## The API

### Creating connections

Creating connections will create a DOM element for each pair of matching endpoints (each <code>from</code> gets connected with every <code>to</code>).
The elements are appended to the specified <code>within</code> container.

#### Connect everything

```javascript
$('div').connections();
```

#### Connect source and destination

```javascript
$('#source, #destination').connections();
```

#### Connect the element with ID 'parent' to each div of class 'child'

```javascript
$().connections({ from: '#parent', to: 'div.child' });
```

#### Connect the element with ID 'parent' to each div of class 'child'; count the divs

```javascript
$('div.child').connections({ from: '#parent' }).length;
```

#### Options

| Option        | Value     | Default   |
| :----------:  | :------:  | :------:  |
| borderClasses | Class names toggled for a created element’s actually visible borders | {} |
| class         | Class name(s) given to each created element | <code>'connection'</code> |
| css           | Inline CSS properties applied to each created element | {} |
| from          | A jQuery selector. Connect each matching element with each <code>to</code> element. | The collection itself |
| tag           | The tag name for created DOM elements. Please note that this is a non-standard HTML element by default. | <code>'connection'</code> |
| to            | A jQuery selector. Connect each from element with <code>each</code> matching element. | The collection itself |
| within        | A jQuery selector. Created elements are appended to the matching element. | <code>':root'</code> |


### Remove connections

The <code>remove</code> method removes the given connections from the DOM.
This method can either be applied to the created connection elements or the connected elements.

```javascript
$('.disabled').connections('remove');
```
### Refresh / update connections

The <code>update</code> method re-calculates the given connections’ position.
This method can either be applied to the created connection elements or the connected elements.

```javascript
$('.moving-part').connections('update');
```

## License

Released under the [MIT license terms](LICENSE.txt).
